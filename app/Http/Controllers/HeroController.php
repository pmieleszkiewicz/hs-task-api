<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\HeroUpdateRequest;
use App\Models\Hero;
use App\Services\HeroServiceInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class HeroController extends Controller
{
    public function __construct(
        private HeroServiceInterface $heroService,
    ) {
    }

    public function index(): View
    {
        return view('heroes.index');
    }

    public function get(Hero $hero): View
    {
        return view('heroes.get', ['hero' => $hero]);
    }

    public function edit(Hero $hero): View
    {
        return view('heroes.edit', ['hero' => $hero]);
    }

    public function update(HeroUpdateRequest $request, int $id): RedirectResponse
    {
        $this->heroService->update($id, $request->validated());

        return back()->with('status', 'Hero has been updated!');
    }
}

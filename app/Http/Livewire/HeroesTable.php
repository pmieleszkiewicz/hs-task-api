<?php

declare(strict_types=1);

namespace App\Http\Livewire;

use App\Models\Hero;
use App\Services\HeroServiceInterface;
use Livewire\Component;
use Livewire\WithPagination;

class HeroesTable extends Component
{
    use WithPagination;

    public string $name = '';
    public string $gender = 'all';

    protected $queryString = [
        'name' => ['except' => ''],
        'gender' => ['except' => 'all']];

    public function updatingName()
    {
        $this->resetPage();
    }

    public function updatingGender()
    {
        $this->resetPage();
    }

    public function render(HeroServiceInterface $heroService)
    {
        $heroes = $heroService->filterAndPaginate($this->name, $this->gender);
        $genders = $heroService->getGenders();

        return view('livewire.heroes-table', ['heroes' => $heroes, 'genders' => $genders]);
    }
}

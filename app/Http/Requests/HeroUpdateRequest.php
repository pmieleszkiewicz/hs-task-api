<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HeroUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'gender' => 'required|string|max:32',
            'mass' => 'nullable|numeric|gt:0',
            'height' => 'nullable|numeric|gt:0',
            'hair_color' => 'required|string|max:64',
            'birth_year' => 'required|string|ends_with:BBY,ABY|max:32',
        ];
    }
}

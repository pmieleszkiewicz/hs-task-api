<?php

declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Models\Hero;
use App\Repositories\HeroRepositoryInterface;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

class HeroRepository extends BaseRepository implements HeroRepositoryInterface
{
    public function __construct(Hero $model)
    {
        parent::__construct($model);
    }

    public function getGenders(): Collection
    {
        return $this->model->select(['gender'])->distinct()->get()->pluck('gender');
    }

    public function filterAndPaginate(string $name, string $gender, int $perPage = 15, array $columns = ['*']): Paginator
    {
        return $this->model->where('name', 'ILIKE', "%{$name}%")
            ->when($gender !== 'all', fn($q) => $q->where('gender', $gender))
            ->paginate($perPage, $columns);
    }
}

<?php

declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Repositories\EloquentRepositoryInterface;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BaseRepository implements EloquentRepositoryInterface
{
    public function __construct(
        protected Model $model
    ) {
    }

    public function all(array $columns = ["*"]): Collection
    {
        return $this->model->get($columns);
    }

    public function paginate(int $perPage = 15, array $columns = ['*']): Paginator
    {
        return $this->model->paginate($perPage, $columns);
    }

    public function create(array $data): Model
    {
        return $this->model->create($data);
    }

    public function update(int $id, array $data, $attribute = 'id'): bool
    {
        return (bool) $this->model->where($attribute, $id)->update($data);
    }

    public function delete(int $id): void
    {
        $this->model->destroy($id);
    }

    public function find(int $id, array $columns = ['*']): ?Model
    {
        return $this->model->find($id, $columns);
    }

    public function findBy(string $field, $value, $columns = ['*']): ?Model
    {
        return $this->model->where($field, $value)->first($columns);
    }
}

<?php

declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface EloquentRepositoryInterface
{
    public function all(array $columns = ["*"]): Collection;
    public function paginate(int $perPage = 15, array $columns = ['*']): Paginator;
    public function create(array $data): Model;
    public function update(int $id, array $data, $attribute = 'id'): bool;
    public function delete(int $id): void;
    public function find(int $id, array $columns = ['*']): ?Model;
    public function findBy(string $field, $value, $columns = ['*']): ?Model;
}

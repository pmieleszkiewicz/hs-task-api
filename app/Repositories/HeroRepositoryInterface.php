<?php

declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

interface HeroRepositoryInterface extends EloquentRepositoryInterface
{
    public function getGenders(): Collection;
    public function filterAndPaginate(string $name, string $gender, int $perPage = 15, array $columns = ['*']): Paginator;
}

<?php

declare(strict_types=1);

namespace App\Services;

use App\Repositories\HeroRepositoryInterface;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class HeroService implements HeroServiceInterface
{
    public function __construct(
        private HeroRepositoryInterface $heroRepository
    ) {
    }

    public function create(array $data): Model
    {
        return $this->heroRepository->create($data);
    }

    public function filterAndPaginate(string $name, string $gender, int $perPage = 15, array $columns = ['*']): Paginator
    {
        return $this->heroRepository->filterAndPaginate($name, $gender, $perPage, $columns);
    }

    public function getGenders(): Collection
    {
        return $this->heroRepository->getGenders();
    }

    public function update(int $id, array $data): bool
    {
        return $this->heroRepository->update($id, $data);
    }
}

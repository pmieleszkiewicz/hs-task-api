<?php

declare(strict_types=1);

namespace App\Services\Clients;

use Illuminate\Http\Client\Factory;
use Illuminate\Support\Collection;

class StarWarsClient implements StarWarsClientInterface
{
    const BASE_URL = 'http://swapi.dev/api';
    const PER_PAGE = 10;

    public function __construct(private Factory $http)
    {
    }

    public function getPeople(int $limit): Collection
    {
        $maxPage = (int) ceil($limit / static::PER_PAGE);
        $itemsOnLastPage = $limit % static::PER_PAGE;

        $people = new Collection();

        for ($page = 1; $page <= $maxPage; $page++) {
            $response = $this->http->get(sprintf("%s/people/?page=%d", static::BASE_URL, $page));
            if (!$response->successful()) {
                throw new \Exception('Failed to fetched Star Wars API');
            }
            $result = $response->json();
            $data = new Collection($result['results']);
            if ($page === $maxPage && $itemsOnLastPage !== 0) {
                $data = $data->slice(0, $itemsOnLastPage);
            }

            // Sanitize data - change 'unknown' to null etc.'
            $data = $data->map(function ($person) {
                $person['mass'] = is_numeric($person['mass']) ? $person['mass'] : null;
                $person['height'] = is_numeric($person['height']) ? $person['height'] : null;
                $person['birth_year'] = $person['birth_year'] !== 'unknown' ? $person['birth_year'] : null;

                return $person;
            });

            $people = $people->merge($data);
        }

        return $people;
    }
}

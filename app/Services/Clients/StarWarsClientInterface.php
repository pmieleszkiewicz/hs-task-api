<?php

declare(strict_types=1);

namespace App\Services\Clients;

use Illuminate\Support\Collection;

interface StarWarsClientInterface
{
    public function getPeople(int $limit): Collection;
}

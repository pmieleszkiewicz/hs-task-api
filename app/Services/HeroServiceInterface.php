<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface HeroServiceInterface
{
    public function filterAndPaginate(string $name, string $gender, int $perPage = 15, array $columns = ['*']): Paginator;
    public function create(array $data): Model;
    public function getGenders(): Collection;
    public function update(int $id, array $data): bool;
}

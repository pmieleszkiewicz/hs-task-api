<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Services\Clients\StarWarsClientInterface;
use App\Services\HeroService;
use App\Services\HeroServiceInterface;
use Illuminate\Console\Command;
use Illuminate\Database\ConnectionInterface;

class FetchHeroes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'heroes:fetch
                            {limit=50 : Number of heroes to fetch}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Star Wars heroes using SW API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        private StarWarsClientInterface $client,
        private HeroServiceInterface $heroService,
        private ConnectionInterface $db,
    ) {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $limit = (int) $this->argument('limit');

        $this->info('Started fetching heroes from Star Wars API...');
        $people = $this->client->getPeople($limit);
        $this->info(sprintf("%d people fetched!", $people->count()));

        $this->db->beginTransaction();
        try {
            $people->each(function ($person) {
                $this->heroService->create($person);
                $this->comment("Hero has been saved: {$person['name']}");
            });
        } catch (\Exception $e) {
            $this->db->rollBack();
            $this->error(sprintf("Failed to fetch: %s", $e->getMessage()));
        }
        $this->db->commit();

        $this->info(sprintf("%d heroes saved!", $people->count()));

        return 0;
    }
}

<?php

declare(strict_types=1);

namespace App\Providers;

use App\Repositories\Eloquent\HeroRepository;
use App\Repositories\HeroRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HeroRepositoryInterface::class, HeroRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

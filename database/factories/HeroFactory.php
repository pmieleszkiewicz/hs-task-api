<?php

namespace Database\Factories;

use App\Models\Hero;
use Illuminate\Database\Eloquent\Factories\Factory;

class HeroFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Hero::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'gender' => $this->faker->randomElement(['male', 'female', 'n/a']),
            'mass' => $this->faker->randomFloat(1, 1, 10000),
            'height' => $this->faker->randomFloat(1, 1, 10000),
            'hair_color' => $this->faker->safeColorName,
            'birth_year' => $this->faker->numberBetween(1, 3000) . $this->faker->randomElement(['BBY', 'ABY']),
        ];
    }

    /**
     * Indicate that the hero's gender is male.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function male()
    {
        return $this->state(function (array $attributes) {
            return [
                'gender' => 'male',
            ];
        });
    }

    /**
     * Indicate that the hero's gender is female.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function female()
    {
        return $this->state(function (array $attributes) {
            return [
                'gender' => 'female',
            ];
        });
    }
}

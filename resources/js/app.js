require('./bootstrap');

require('alpinejs');

const bootstrapThemeMode = () => {
    if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
        document.documentElement.classList.add('dark')
    } else {
        document.documentElement.classList.remove('dark')
    }
}

const toggleThemeMode = () => {
    if (localStorage.theme === 'light') {
        localStorage.theme = 'dark';
        document.documentElement.classList.add('dark');
    } else {
        localStorage.theme = 'light';
        document.documentElement.classList.remove('dark');
    }
}

window.onload = () => {
    bootstrapThemeMode();
    document.getElementById("toggle-theme")?.addEventListener("click", toggleThemeMode);
    document.getElementById("toggle-theme-responsive")?.addEventListener("click", toggleThemeMode);
}


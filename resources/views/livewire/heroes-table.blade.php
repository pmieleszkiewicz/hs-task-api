<div class="flex flex-col space-y-2">
    <div class="flex justify-between items-center">

        <!-- Name search imput -->
        <div class="w-full md:w-1/3 pr-3 mb-3 md:mb-0">
            <label class="input-label mb-1">
                Name
            </label>
            <input wire:model.debounce.500ms="name" name="name" class="input" type="text" placeholder="Search by name...">
        </div>

        <!-- Gender dropdown -->
        <div class="w-full md:w-1/3 pl-3 mb-3 md:mb-0">
            <label class="input-label" for="gender-input">
                Gender
            </label>
            <div class="relative">
                <select wire:model="gender" name="gender" class="input" id="gender-input">
                    <option value="all">all</option>
                    @foreach ($genders as $gender)
                        <option wire:click="$set('gender', $gender')" value="{{ $gender }}">{{ $gender }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="overflow-x-auto">
        <table class="min-w-max w-full table-auto shadow-md rounded-lg overflow-hidden bg-white dark:bg-gray-800">
            @if (!$heroes->isEmpty())
                <thead>
                <tr class="bg-gray-200 dark:bg-gray-900 text-gray-600 dark:text-gray-100 uppercase text-sm leading-normal">
                    <th class="py-3 px-6 text-left">ID</th>
                    <th class="py-3 px-6 text-left">Name</th>
                    <th class="py-3 px-6 text-center">Gender</th>
                    <th class="py-3 px-6 text-center">Mass (kg)</th>
                    <th class="py-3 px-6 text-center">Height (cm)</th>
                    <th class="py-3 px-6 text-center">Hair color</th>
                    <th class="py-3 px-6 text-center">Birth year</th>
                    <th class="py-3 px-6 text-center">Options</th>
                </tr>
                </thead>
            @endif
            <tbody class="text-gray-600 text-sm font-light">

            @forelse ($heroes as $hero)
                <tr class="border-b border-gray-200 dark:border-gray-900 hover:bg-gray-100 dark:hover:bg-gray-900 dark:text-gray-100">
                    <td class="py-3 px-6 text-left whitespace-nowrap">
                        {{ $hero->id }}
                    </td>
                    <td class="py-3 px-6 text-left">
                        {{ $hero->name }}
                    </td>
                    <td class="py-3 px-6 text-center">
                            <span
                                class="badge {{ $hero->isMale() ? 'bg-blue-200 text-blue-600' : ($hero->isFemale() ? 'bg-pink-200 text-pink-600' : 'bg-gray-200 text-gray-600') }}">
                                {{ $hero->gender }}
                            </span>
                    </td>
                    <td class="py-3 px-6 text-center">
                        {{ $hero->mass ?? '-' }}
                    </td>
                    <td class="py-3 px-6 text-center">
                        {{ $hero->height ?? '-' }}
                    </td>
                    <td class="py-3 px-6 text-center">
                        {{ $hero->hair_color }}
                    </td>
                    <td class="py-3 px-6 text-center">
                        {{ $hero->birth_year ?? '-' }}
                    </td>
                    <td>
                        <div class="flex justify-center items-center">
                            <a href="{{ route('heroes.get', ['hero' => $hero->id]) }}" class="w-4 mx-1 transform hover:text-blue-500 dark:hover:text-red-500 hover:scale-110 cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"/>
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"/>
                                </svg>
                            </a>
                            <a href="{{ route('heroes.edit', ['hero' => $hero->id]) }}" class="w-4 mx-1 transform hover:text-blue-500 dark:hover:text-red-500 hover:scale-110 cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"/>
                                </svg>
                            </a>
                        </div>
                    </td>
                </tr>
            @empty
                <h1 class="text-lg font-bold text-gray-800 dark:text-gray-200">Heroes not found!</h1>
            @endforelse
            </tbody>
        </table>
    </div>
    @if (!$heroes->isEmpty())
        {{ $heroes->links('livewire.pagination.tailwind') }}
    @endif
</div>

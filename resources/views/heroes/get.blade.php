<x-app-layout>
    @section('title')
        {{ $hero->name }}
    @endsection

    <div class="py-3 md:py-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="flex justify-between items-center mb-2">
                <h1 class="text-2xl font-bold uppercase text-gray-700 dark:text-gray-200">{{ $hero->name }}</h1>
                <a href="{{ route('heroes.edit', ['hero' => $hero->id]) }}" class="btn text-base px-3 py-1.5">Edit</a>
            </div>
            <div class="flex flex-col space-y-1 bg-white dark:bg-gray-800 dark:text-gray-300 rounded-md p-4">
                <div>
                    <span class="font-bold">Gender:</span>
                    {{ $hero->gender }}
                </div>
                <div>
                    <span class="font-bold">Mass (kg):</span>
                    {{ $hero->mass }}
                </div>
                <div>
                    <span class="font-bold">Height (cm):</span>
                    {{ $hero->height }}
                </div>
                <div>
                    <span class="font-bold">Hair color:</span>
                    {{ $hero->hair_color }}
                </div>
                <div>
                    <span class="font-bold">Birth year:</span>
                    {{ $hero->birth_year }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<x-app-layout>
    @section('title')
        {{ $hero->name }}
    @endsection

    <div class="py-3 md:py-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (session('status'))
                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative mb-4" role="alert">
                    <strong class="font-bold">Success!</strong>
                    <span class="block sm:inline">{{ session('status') }}</span>
                </div>
            @endif

            <h1 class="text-2xl font-bold uppercase text-gray-700 dark:text-gray-200 mb-2">{{ $hero->name }}</h1>

            <form action="{{ route('heroes.update', ['id' => $hero->id]) }}" method="POST">
                @method('PATCH')
                @csrf

                <div class="flex flex-col space-y-3 dark:text-gray-300 mb-3">

                    <!-- Name -->
                    <div>
                        <label class="input-label mb-1">Name</label>
                        <input name="name" class="input {{ $errors->has('name') ? 'has-error' : '' }}" type="text" value="{{ old('name') ?? $hero->name }}">
                        @error('name')
                            <div class="text-red-500">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Gender -->
                    <div>
                        <label class="input-label mb-1">Gender</label>
                        <input name="gender" class="input {{ $errors->has('gender') ? 'has-error' : '' }}" type="text" value="{{ old('gender') ?? $hero->gender }}">
                        @error('gender')
                            <div class="text-red-500">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Mass -->
                    <div>
                        <label class="input-label mb-1">Mass (kg)</label>
                        <input name="mass" class="input {{ $errors->has('mass') ? 'has-error' : '' }}" type="number" value="{{ old('mass') ?? $hero->mass }}">
                        @error('mass')
                            <div class="text-red-500">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Height -->
                    <div>
                        <label class="input-label mb-1">Height (cm)</label>
                        <input name="height" class="input {{ $errors->has('height') ? 'has-error' : '' }}" type="number" value="{{ old('height') ?? $hero->height }}">
                        @error('height')
                            <div class="text-red-500">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Hair color -->
                    <div>
                        <label class="input-label mb-1">Hair color</label>
                        <input name="hair_color" class="input {{ $errors->has('hair_color') ? 'has-error' : '' }}" type="text" value="{{ old('hair_color') ?? $hero->hair_color }}">
                        @error('hair_color')
                            <div class="text-red-500">{{ $message }}</div>
                        @enderror
                    </div>

                    <!-- Birth year -->
                    <div>
                        <label class="input-label mb-1">Birth year</label>
                        <input name="birth_year" class="input {{ $errors->has('birth_year') ? 'has-error' : '' }}" type="text" value="{{ old('birth_year') ?? $hero->birth_year }}">
                        @error('birth_year')
                            <div class="text-red-500">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="flex justify-end">
                    <a href="{{ route('heroes.get', ['hero' => $hero->id]) }}" class="px-3 py-2 text-base dark:text-gray-100">Back</a>
                    <button type="submit" class="btn px-3 py-2 text-base">Save</button>
                </div>
            </form>

        </div>
    </div>
</x-app-layout>

<x-app-layout>
    @section('title')
        Heroes
    @endsection

    <div class="py-3 md:py-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <livewire:heroes-table />
        </div>
    </div>
</x-app-layout>

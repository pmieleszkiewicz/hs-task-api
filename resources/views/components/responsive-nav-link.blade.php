@props(['active'])

@php
$classes = ($active ?? false)
            ? 'block pl-3 pr-4 py-2 border-l-4 border-blue-400 dark:border-red-400 text-base font-medium text-blue-700 dark:text-red-500 bg-gray-100 dark:bg-gray-800 focus:outline-none focus:text-blue-800 dark:focus:text-red-800 focus:bg-blue-100 dark:focus:bg-gray-900 focus:border-blue-700 dark:focus:border-red-700 transition duration-150 ease-in-out'
            : 'block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 dark:text-gray-200 hover:text-gray-800 hover:bg-gray-50 dark:hover:bg-gray-900 hover:border-blue-500 dark:hover:border-red-500 focus:outline-none focus:text-gray-800 focus:bg-gray-50 dark:focus:bg-gray-900 focus:border-blue-700 dark:focus:border-red-700 transition duration-150 ease-in-out';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>

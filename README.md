<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Description
Project requires PHP 8 and uses following packages:
- **Laravel Sail** - to set up Docker environment
- **Laravel Breeze** - for simple auth implementation
- **Laravel Livewire** - I've tried FE as Vue SPA and Inertia.js, but never Livewire, so it was a great opportunity to test it in action
- **Tailwind CSS**

## Installation
Rename `.env.docker` to `.env`.
### Using Laravel Sail
```bash
# Install Composer packages
composer install

# Run Docker containers
./vendor/bin/sail up -d

# Install npm packages and build assets
./vendor/bin/sail npm install
./vendor/bin/sail npm run dev

# Regenerate app secret key
./vendor/bin/sail artisan key:generate

# Migrate database and create sample user
./vendor/bin/sail artisan migrate --seed

# Fetch people from Star Wars API using custom Artisan Command
./vendor/bin/sail artisan heroes:fetch {int:limit}
```

To log in as user you can use credentials below:
```bash
john.doe@example.com
password
```
Application is running on: [http://localhost:8000](http://localhost:8000)<br>
MailHog is running on: [http://localhost:8025](http://localhost:8025)

## Testing

```bash
./vendor/bin/sail test
```

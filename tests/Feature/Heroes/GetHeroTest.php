<?php

namespace Tests\Feature\Heroes;

use App\Models\Hero;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetHeroTest extends TestCase
{
    use RefreshDatabase;

    public function test_when_not_authenticated()
    {
        $response = $this->get('/heroes/1', []);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
        $response->assertLocation('/login');
    }

    public function test_when_hero_does_not_exist()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)
            ->get(sprintf('/heroes/%d', 1));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function test_when_hero_exists()
    {
        $user = User::factory()->create();
        $hero = Hero::factory()->create();

        $response = $this->actingAs($user)
            ->get(sprintf('/heroes/%d', $hero->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($hero->name);
    }
}

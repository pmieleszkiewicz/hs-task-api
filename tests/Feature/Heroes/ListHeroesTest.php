<?php

namespace Tests\Feature\Heroes;

use App\Http\Livewire\HeroesTable;
use App\Models\Hero;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Livewire\Livewire;
use Tests\TestCase;

class ListHeroesTest extends TestCase
{
    use RefreshDatabase;

    public function test_when_not_authenticated()
    {
        $response = $this->get('/heroes');

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
        $response->assertLocation('/login');
    }

    public function test_search_by_name_and_filer_by_gender()
    {
        Hero::factory()->male()->create(['name' => 'Anakin Skywalker']);
        Hero::factory()->male()->create(['name' => 'Luke Skywalker']);
        Hero::factory()->female()->create(['name' => 'Shmi Skywalker']);
        Hero::factory()->male()->create(['name' => 'John Walker']);
        Hero::factory()->female()->create(['name' => 'Kate Walker']);
        Hero::factory()->male()->create(['name' => 'Yoda']);

        Livewire::withQueryParams(['name' => 'walk', 'gender' => 'male'])
            ->test(HeroesTable::class)
            ->assertSee('Anakin Skywalker')
            ->assertSee('Luke Skywalker')
            ->assertSee('John Walker')
            ->assertDontSee('Kate Walker')
            ->assertDontSee('Shmi Skywalker')
            ->assertDontSee('Yoda');
    }
}

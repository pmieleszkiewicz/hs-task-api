<?php

namespace Tests\Feature\Heroes;

use App\Models\Hero;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateHeroTest extends TestCase
{
    use RefreshDatabase;

    public function test_when_not_authenticated()
    {
        $response = $this->patch('/heroes/1', []);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
        $response->assertLocation('/login');
    }

    public function test_with_all_fields_empty()
    {
        $user = User::factory()->create();
        $hero = Hero::factory()->create();

        $response = $this->actingAs($user)
            ->patch(sprintf('/heroes/%d', $hero->id), []);

        $response->assertSessionHasErrors(['name', 'gender', 'hair_color', 'birth_year']);
    }

    public function test_with_birth_year_field_invalid_format()
    {
        $user = User::factory()->create();
        $hero = Hero::factory()->create();

        $response = $this->actingAs($user)
            ->patch(sprintf('/heroes/%d', $hero->id), [
                'name' => 'Name',
                'gender' => 'male',
                'mass' => 10,
                'height' => 50,
                'hair_color' => 'blonde',
                'birth_year' => '1000'
            ]);

        $response->assertSessionHasErrors(['birth_year']);
    }

    public function test_with_all_fields_valid()
    {
        $user = User::factory()->create();
        $hero = Hero::factory()->create();

        $response = $this->actingAs($user)
            ->from(route('heroes.edit', ['hero' => $hero->id]))
            ->patch(sprintf('/heroes/%d', $hero->id), [
                'name' => 'Name',
                'gender' => 'male',
                'mass' => 10,
                'height' => 50,
                'hair_color' => 'blonde',
                'birth_year' => '1000BBY'
            ]);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('heroes', [
            'name' => 'Name',
            'gender' => 'male',
            'mass' => 10,
            'height' => 50,
            'hair_color' => 'blonde',
            'birth_year' => '1000BBY'
        ]);
        $response->assertRedirect(route('heroes.edit', ['hero' => $hero->id]));
    }

    public function test_with_mass_is_negative_number()
    {
        $user = User::factory()->create();
        $hero = Hero::factory()->create();

        $response = $this->actingAs($user)
            ->from(route('heroes.edit', ['hero' => $hero->id]))
            ->patch(sprintf('/heroes/%d', $hero->id), [
                'name' => 'Name',
                'gender' => 'male',
                'mass' => -10,
                'height' => 50,
                'hair_color' => 'blonde',
                'birth_year' => '1000BBY'
            ]);

        $response->assertSessionHasErrors(['mass']);
    }

    public function test_with_height_is_negative_number()
    {
        $user = User::factory()->create();
        $hero = Hero::factory()->create();

        $response = $this->actingAs($user)
            ->from(route('heroes.edit', ['hero' => $hero->id]))
            ->patch(sprintf('/heroes/%d', $hero->id), [
                'name' => 'Name',
                'gender' => 'male',
                'mass' => 10,
                'height' => -50,
                'hair_color' => 'blonde',
                'birth_year' => '1000BBY'
            ]);

        $response->assertSessionHasErrors(['height']);
    }
}

<?php

use App\Http\Controllers\HeroController;
use Illuminate\Support\Facades\Route;

Route::get('/', fn() => redirect()->route('heroes.index'));

Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'heroes', 'as' => 'heroes.'], function () {
        Route::get('', [HeroController::class, 'index'])->name('index');
        Route::patch('{id}', [HeroController::class, 'update'])->name('update');
        Route::get('{hero}', [HeroController::class, 'get'])->name('get');
        Route::get('{hero}/edit', [HeroController::class, 'edit'])->name('edit');
    });
});

require __DIR__.'/auth.php';
